export const OrderRowMenu = ({ item }) => {
    const meal = item.details.meal;
    const dessert = item.details.dessert;

    return (
        <tr>

            <td>
                {item.title}
                <ul>
                    <li>{meal}</li>
                    <li>{dessert}</li>
                </ul>
            </td>
            <td>{item.price.toFixed(2)} €</td>
        </tr>
    )
}