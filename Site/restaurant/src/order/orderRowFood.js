export const OrderRowFood = ({ item }) => {
    return (
        <tr>
            <td>{item.title}</td>
            <td>{item.price.toFixed(2)} €</td>
        </tr>
    )
}