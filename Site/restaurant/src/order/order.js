import { useContext } from 'react';
import { Modal } from 'react-simple-modal-cb';
import { CartContext } from './../context/cart-context';
import { OrderRowFood } from './orderRowFood';
import { OrderRowMenu } from './orderRowMenu';

export const Order = () => {
    const [cart, setCart] = useContext(CartContext);
    const rowsFoods = cart.selectedFoods.map((item) => <OrderRowFood item={item} />);
    const rowsMenus = cart.selectedMenus.map((item) => <OrderRowMenu item={item} />);
    const total = cart.selectedFoods.reduce(((resultat, selected) => resultat + selected.price), 0) + cart.selectedMenus.reduce(((resultat, selected) => resultat + selected.price), 0);
    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Article</th>
                        <th scope="col">Prix</th>
                    </tr>
                </thead>
                <tbody>
                    {rowsMenus}
                    {rowsFoods}
                </tbody>
                <thead>
                    <tr>
                        <th scope="col">Total</th>
                        <th scope="col">{total.toFixed(2)} €</th>
                    </tr>
                </thead>
            </table>
            <button className="btn btn-primary mx-3" disabled={cart.selectedFoods.length + cart.selectedMenus.length === 0} onClick={handleSend(cart)}>Valider la commande</button>
        </div>
    )
}




const handleSend = (cart) => (e) => {
    let command = { ...cart };
    command['id'] = Math.random() * 10000000000;
    command['description'] = cart.selectedFoods.reduce(((resultat, selected) => resultat + selected.title + " (" + selected.price + " €)\n"), "") + cart.selectedMenus.reduce(((resultat, selected) => resultat + selected.title + " (" + selected.price + " €) : " + selected.details.meal + " , " + selected.details.meal + "\n"), "");
    command['price'] = cart.selectedFoods.reduce(((resultat, selected) => resultat + selected.price), 0) + cart.selectedMenus.reduce(((resultat, selected) => resultat + selected.price), 0);
    command['date'] = new Date();
    command['client'] = "Maxime POULAIN";

    let request = new Request("http://cabe0232.odns.fr/webdev-api/order", {
        method: 'POST',
        body: JSON.stringify(command),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    });

    fetch(request).then((res) => {
        if (res.status >= 200 && res.status < 400) {
            window.alert("Commande validée");
        }
        else {
            window.alert("Erreur lors de la commande");
        }
    }
    ).catch(() =>
        window.alert("Erreur lors de la commande")
    );
}




