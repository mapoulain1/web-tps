import { useContext } from 'react';
import { useState } from 'react';
import { CartContext } from './../context/cart-context';





export const MenuCard = ({ menu, foodList }) => {
    const [cart, setCart] = useContext(CartContext);
    const [selected, setSelected] = useState({ meal: null, dessert: null });
    let mealsCheckbox = foodList.filter((food) => food.category === 'PLAT' && food.menuId === menu.id).map((food) => <MenuCardCheckbox menu={menu} food={food} type="meal" selected={selected} setSelected={setSelected} />);
    let dessertsCheckbox = foodList.filter((food) => food.category === 'DESSERT').map((food) => <MenuCardCheckbox menu={menu} food={food} type="dessert" selected={selected} setSelected={setSelected} />);
    return (
        <div className="card w-25 mx-3 mb-4">
            <div className="card-header">
                <h4 className="card-title">{menu.title}</h4>
            </div>
            <div className="card-body d-flex flex-column">
                <div className="flex-grow-1">
                    <h6>Choisir un plat :</h6>
                    <div>{mealsCheckbox}</div>
                </div>
                <div className="mt-3">
                    <h6>Choisir un dessert :</h6>
                    <div>{dessertsCheckbox}</div>
                </div>
                <div className="d-flex mt-3">
                    <span className="fw-bold">{menu.price}€</span>
                    <button id={"addMenuToCart-" + menu.id} className="btn btn-primary mx-3" onClick={() => addMenuToCart(menu, cart, setCart, selected)} disabled={!selected.dessert || !selected.meal}>Ajouter à ma commande</button>
                </div>
            </div>
        </div>
    );
};
const addMenuToCart = (menu, cart, setCart, selected) => {
    const newCart = { ...cart };
    const newMenu = { ...menu };
    newMenu['details'] = selected;
    newCart.selectedMenus.push(newMenu);
    setCart(newCart);
}


const handleChangeMeal = (e, selected, setSelected) => {
    const newSelected = { ...selected };
    newSelected.meal = document.getElementById("label-" + e).innerHTML;
    setSelected(newSelected);
}
const handleChangeDessert = (e, selected, setSelected) => {
    const newSelected = { ...selected };
    newSelected.dessert = document.getElementById("label-" + e).innerHTML;
    setSelected(newSelected);
}
const handleCheckboxs = (menu, selected, setSelected, type) => (e) => {
    const id = e.target.id;
    if (type === "meal")
        handleChangeMeal(id, selected, setSelected);
    else
        handleChangeDessert(id, selected, setSelected);
}


export const MenuCardCheckbox = ({ menu, food, type, selected, setSelected }) => {
    return (
        <div key={food.id} className="form-check" onChange={handleCheckboxs(menu, selected, setSelected, type)}>
            <input className="form-check-input" type="radio" name={`menu-${menu.id}-${food.category}`} id={`menu-${menu.id}-${food.id}`} />
            <label id={`label-menu-${menu.id}-${food.id}`} className="form-check-label" htmlFor={`menu-${menu.id}-${food.id}`}>
                {food.title}
            </label>
        </div>
    );
}