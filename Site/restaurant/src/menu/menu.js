import './menu.css'
import { MenuCard } from './card'

export const Menu = ({ foodList, menuList }) => {
    const menuCards = menuList.map((menu) => <MenuCard key={`menu-${menu.id}`} menu={menu} foodList={foodList} />);
    return (
        <div className='d-flex flex-wrap'>
            {menuCards}
        </div>
    )
}
