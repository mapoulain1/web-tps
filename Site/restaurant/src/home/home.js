import './home.css'
import kebab from './../kebab.png';


const description = "Plat oriental composé de brochettes de viande, généralement de mouton, rôties à feu vif et fortement épicées. L'Arménien, hôte du logis, étendit une peau de bœuf sur le tapis, et apporta successivement des amandes grillées, ce qui excite à boire, du fromage blanc, du pain et des brochettes de kébab ou filet de mouton rôti entre des fragments de graisse et des feuilles de laurier, le nec plus ultra de la délicatesse.";
    
export const Home = () => {
    
    return (
        
        <div className='d-flex flex-row homePage'>
            <img src={kebab} alt='kebab' className='zozanImage'/>
            <p className='description'>
                {description}
            </p>
        </div>
    )
}

