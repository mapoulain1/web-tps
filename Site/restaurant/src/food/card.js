import { useContext } from 'react';
import { CartContext } from './../context/cart-context';


export const Card = ({ food }) => {
    const [cart, setCart] = useContext(CartContext);
    return (
        <div key={food.id} className="card w-25 mx-3 mb-4">
            <img src={food.photo} className="card-img-top" alt={food.title} />
            <div className="card-body d-flex flex-column">
                <h5 className="card-title flex-grow-1">{food.title}</h5>
                <p className="card-text flex-grow-1">{food.description}</p>
                <span className="fw-bold mb-3 align-self-center">{food.price}€</span>
                <button href="#" className="btn btn-primary mx-3" onClick={() => addFoodToCart(food, cart, setCart)}>Ajouter à ma commande</button>
            </div>
        </div>
    );
}

const addFoodToCart = (food, cart, setCart) => {
    const newCart = { ...cart };
    newCart.selectedFoods.push(food);
    setCart(newCart);
}
