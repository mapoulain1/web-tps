import { Card } from './card';

export const Food = ({foodList}) => {
    const mealCards = foodList
        .filter((food) => food.category === 'PLAT')
        .map((food) => <Card key={food.id} food={food}  />);

    const dessertCards = foodList
        .filter((food) => food.category === 'DESSERT')
        .map((food) => <Card key={food.id} food={food}/>);

        
        return (
        <div className='d-flex flex-column'>
            <h2>Plats</h2>
            <div className="d-flex flex-wrap">{mealCards}</div>
            
            <h2>Desserts</h2>
            <div className="d-flex flex-wrap">{dessertCards}</div>
        </div>
    );
};


