import './App.css';
import { Header } from './header/header'
import { Home } from './home/home'
import { Menu } from './menu/menu'
import { Food } from './food/food'
import { Order } from './order/order';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from 'react';
import 'bootstrap-icons/font/bootstrap-icons.css';
import {CartContext} from './context/cart-context';
import { Admin } from './admin/admin';

function App() {
	const [foodList, setFoodList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/food').then(response => response.json())
			.then(json => { setFoodList(json) });
	}, []);


	const [menuList, setMenuList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/menu').then(response => response.json())
			.then(json => { setMenuList(json) });
	}, []);

	const [cart, setCart] = useState({ selectedFoods: [], selectedMenus: [] });

	return (

		<BrowserRouter>
			<CartContext.Provider value={[cart, setCart]}>
				<Header className="container" />
				<main className="w-75 mx-auto p-5">
					<Routes>
						<Route>
							<Route path="*" element={<Home />} />
							<Route path="menu" element={<Menu foodList={foodList} menuList={menuList} />} />
							<Route path="food" element={<Food foodList={foodList} />} />
							<Route path="order" element={<Order />} />
							<Route path="admin" element={<Admin />} />
						</Route>
					</Routes>
				</main>
			</CartContext.Provider>
		</BrowserRouter>

	);
}




export default App;
