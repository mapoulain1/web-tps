import logo from './../logo.png';
import './header.css'
import 'bootstrap/dist/css/bootstrap.css'
import { Link } from "react-router-dom";
import { useContext } from 'react';
import { CartContext } from './../context/cart-context';





const menus = [
    {
        name: 'Accueil',
        url: '/home',
        icon: 'bi bi-house',
    },
    {
        name: 'Carte',
        url: '/food',
        icon: 'bi bi-bookmark-check',
    },
    {
        name: 'Menus',
        url: '/menu',
        icon: 'bi bi-collection'
    },
    {
        name: 'Commande',
        url: '/order',
        icon: 'bi bi-bag-check'
    },
    {
        name: 'Admin',
        url: '/admin',
        icon: 'bi bi-shield-lock'
    },
];

const navItems = menus.map(menu => {
    return (

        <li className="nav-item mx-3 d-flex align-items-center" key={menu.url}>
            <i className={menu.icon} />
            <span>
                <Link className="nav-link active" aria-current="page" to={menu.url}>{menu.name}</Link>
            </span>
        </li>
    );
});

export const Header = () => {
    const [cart, setCart] = useContext(CartContext);
    return (
        <nav className="navbar navbar-expand-md navbar-light bg-light">
            <Link to="home">
                <img src={logo} alt="logo" className="navbar-brand mx-5 kebabLogo" />
            </Link>
            <ul className="navbar-nav flex-grow-1 d-flex flex-row justify-content-end mx-5 mb-2">
                {navItems}
                <i className="bi-basket position-relative fs-3">
                    <span className="position-absolute badge rounded-pill bg-primary" style={{ fontSize: '0.4em' }}>
                        {cart.selectedFoods.length + cart.selectedMenus.length}
                    </span>
                </i>
            </ul>
        </nav>
    )
}