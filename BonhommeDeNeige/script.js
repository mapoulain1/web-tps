
generateAllFlocons(100);
setInterval(descendreFlocon,0.1);

function generateFlocon(x, y) {
    var tag = document.createElement("div");
    tag.classList.add("flocon");
    tag.style.left = x + "px";
    tag.style.top = y + "px";
    document.body.appendChild(tag);
}


function generateAllFlocons(number) {
    for (let i = 0; i < number; i++) {
        var x = Math.random() * window.innerWidth;
        var y = -Math.random() * window.innerHeight;
        generateFlocon(x,y);
        
    }
}


let speed = 1;

function descendreFlocon(){
    const flocons = document.querySelectorAll(".flocon");
    flocons.forEach(flocon => {
        var top = parseInt(flocon.style.top, 10);
        if(top>window.innerHeight){
            top=-80;
        }

        console.log(top+" : " + window.innerHeight);
        flocon.style.top=top+speed+"px";
        
    });
}

function increaseSpeed(){
    speed++;
}