import { CarCard } from "../cars/car-card";

export const Home = ({ cars }) => {
    const allCars = cars.map((brand) =>
        brand.models.map((model) =>
            model.cars.map((car) =>
                <CarCard car={car} brandName={brand.name} />
            )
        )
    );


    return (
        <div className="d-flex flex-wrap justify-content-between">{allCars}</div>
    );
}