import { useContext } from "react";
import { FavoriteContext } from "../../context/favorite-context";
import { CarCard } from "../cars/car-card";

export const Favorites = () => {
    const [favorite, setFavorite] = useContext(FavoriteContext);
    const allCars = favorite.cars.map((car) =>
        <CarCard car={car} brandName={car.brand} />
    );

    return (
        <div>
            <h2 className="text-light">{`Favorties (${allCars.length})`}</h2>
            <div className="d-flex flex-wrap justify-content-between">{allCars}</div>
        </div>
    );
}