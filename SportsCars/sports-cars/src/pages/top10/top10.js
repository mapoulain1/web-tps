import { CarCard } from "../cars/car-card"

export const Top10 = ({ weightCars, powerCars, ratioCars }) => {

    const weights = weightCars.map((brand) =>
        brand.models.map((model) =>
            model.cars.map((car) =>
                <CarCard car={car} brandName={brand.name} />
            )
        )
    );
    const powers = powerCars.map((brand) =>
        brand.models.map((model) =>
            model.cars.map((car) =>
                <CarCard car={car} brandName={brand.name} />
            )
        )
    );
    const ratios = ratioCars.map((brand) =>
        brand.models.map((model) =>
            model.cars.map((car) =>
                <CarCard car={car} brandName={brand.name} />
            )
        )
    );


    console.log(ratioCars);
    return (
        <div className="d-flex flex-column">
            <div>
                <h2 className="text-light">Top 10 Weight</h2>
                <div className="d-flex flex-wrap justify-content-between">{weights}</div>
            </div>

            <div id="collapsePower">
                <h2 className="text-light">Top 10 Power</h2>
                <div className="d-flex flex-wrap justify-content-between">{powers}</div>
            </div>

            <div id="collapseRatio">
                <h2 className="text-light">Top 10 Ratio</h2>
                <div className="d-flex flex-wrap justify-content-between">{ratios}</div>
            </div>
        </div>

    )
}