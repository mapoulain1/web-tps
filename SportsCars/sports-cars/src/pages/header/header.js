import { Link } from "react-router-dom";
import { useLocation } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'



const menus = [
    {
        name: 'Home',
        url: '/home',
    },
    {
        name: 'Top 10',
        url: '/top10',
    },
    {
        name: 'Favorties',
        url: '/favorites',
    },
];

const navItems = menus.map(menu => {
    return (

        <li className="nav-item mx-3 d-flex align-items-center" key={menu.url}>
            <span>
                <Link className="nav-link active" aria-current="page" to={menu.url}><h4 className="text-light">{menu.name}</h4></Link>
            </span>
        </li>
    );
});



export const Header = ({ onSearchChange }) => {
    const location = useLocation().pathname;
    const searchDisabled = location !== "/home";
    return (
        <nav className="navbar navbar-expand-md navbar-light bg-transparent">
            <Link to="home" className="nav-link active" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                <h2 className="text-light">Sports Cars</h2>
            </Link>

            <form className="form-inline my-2 my-lg-0 ml-5">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" onChange={(event) => onSearchChange(event.target.value)} disabled={searchDisabled} />
            </form>
            <ul className="navbar-nav flex-grow-1 d-flex flex-row justify-content-end mx-5 mb-2">
                {navItems}
            </ul>
        </nav>
    )
}
