import { useContext } from "react";
import { ProgressBar } from "react-bootstrap"
import { FavoriteContext } from '../../context/favorite-context';


export const CarCard = ({ car, brandName }) => {
    const [favorite, setFavorite] = useContext(FavoriteContext);

    const maxWeight = 2200;
    const maxPower = 500;
    const maxRatio = 2;

    return (
        <div key={car.id} className="card w-25 mx-3 mb-4">
            <img src={car.imageUrl} className="card-img-top" alt={car.name} />
            <div className="card-body d-flex flex-column">
                <h5 className="card-title flex-grow-1">{brandName} {car.name}</h5>


                <ul className="list-group list-group-flush">
                    <li className="list-group-item" key={`${car.id}-weight`}>
                        Weight :
                        <ProgressBar variant="warning" now={car.weight / maxWeight * 100} label={`${car.weight} kg`} />
                    </li>
                    <li className="list-group-item" key={`${car.id}-power`}>
                        Power :
                        <ProgressBar variant="error" now={car.power / maxPower * 100} label={`${car.power} hp`} />
                    </li>
                    <li className="list-group-item" key={`${car.id}-ratio`}>
                        Ratio
                        <ProgressBar variant="info" now={maxRatio * 100 / (car.weight / car.power)} label={`${(car.weight / car.power).toFixed(2)} kg/hp`} />
                    </li>
                </ul>
                <button type="button" className="btn btn-danger" onClick={() => addToFavorite(car, brandName, favorite, setFavorite)}>
                    <i className={`bi bi-star${favorite.ids.includes(car.id) ? '-fill' : ''} m-3`} />Add to favorite
                </button>
            </div>
        </div>

    )
}

const addToFavorite = (car, brand, favorite, setFavorite) => {
    const newFavorite = { ...favorite };
    if (favorite.ids.includes(car.id)) {
        const index = favorite.ids.indexOf(car.id);
        favorite.ids.splice(index, 1);
        favorite.cars.splice(index, 1);
    }
    else {
        car['brand'] = brand;
        favorite.ids.push(car.id);
        favorite.cars.push(car);
    }
    setFavorite(newFavorite);
}

