import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from 'react';
import { FavoriteContext } from './context/favorite-context';
import { Home } from './pages/home/home';
import { Header } from './pages/header/header';
import { Top10 } from './pages/top10/top10';
import { Favorites } from './pages/favorites/favorites';



function App() {
	const [carsList, setCarsList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/sportscars').then(response => response.json()).then(json => setCarsList(json));
	}, []);

	const [weightCarsList, setWeightCarsList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/sportscars?sort=weight').then(response => response.json()).then(json => setWeightCarsList(json));
	}, []);

	const [powerCarsList, setPowerCarsList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/sportscars?sort=power').then(response => response.json()).then(json => setPowerCarsList(json));
	}, []);

	const [ratioCarsList, setRatioCarsList] = useState([]);
	useEffect(() => {
		fetch('http://cabe0232.odns.fr/webdev-api/sportscars?sort=ratio').then(response => response.json()).then(json => setRatioCarsList(json));
	}, []);


	const [favorite, setFavorite] = useState({ ids: [], cars: [] });


	const searchChanged = (value) => {
		if (value === "") {
			fetch('http://cabe0232.odns.fr/webdev-api/sportscars').then(response => response.json()).then(json => setCarsList(json));
		}
		else {
			fetch(`http://cabe0232.odns.fr/webdev-api/sportscars?search=${value}`).then(response => response.json()).then(json => setCarsList(json));
		}
	}


	return (
		<BrowserRouter>

			<FavoriteContext.Provider value={[favorite, setFavorite]}>
				<Header className="container" onSearchChange={searchChanged} />
				<main className="w-75 mx-auto p-5">
					<Routes>
						<Route>
							<Route path="*" element={<Home cars={carsList} />} />
							<Route path="top10" element={<Top10 weightCars={weightCarsList} powerCars={powerCarsList} ratioCars={ratioCarsList} />} />
							<Route path="favorites" element={<Favorites />} />
						</Route>
					</Routes>
				</main>
			</FavoriteContext.Provider>
		</BrowserRouter>

	);
}

export default App;
